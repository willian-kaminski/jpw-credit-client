FROM openjdk:8-jdk-alpine

WORKDIR /app

COPY target/JpwCreditClient-0.0.1-SNAPSHOT.jar /app/jpw-service-client.jar

ENTRYPOINT ["java", "-jar", "jpw-service-client.jar"]