package com.br.jpw.JpwCreditClient.controller;

import com.br.jpw.JpwCreditClient.domain.Cliente;
import com.br.jpw.JpwCreditClient.domain.ClienteExistDTO;
import com.br.jpw.JpwCreditClient.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/cliente")
@Api(tags = "cliente (Clientes)", description = "Localização: Cadastros >> Clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping
    @ApiOperation(value = "Retorna uma lista paginada de clientes")
    public ResponseEntity<Page<Cliente>> listarTodos(Pageable pageable){
        return clienteService.listarTodos(pageable);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Retorna um cliente através do Id")
    public ResponseEntity<Cliente> listarTodos(@PathVariable Long id){
        return clienteService.listarPorId(id);
    }

    @PostMapping
    @ApiOperation(value = "Registra um novo cliente na base de dados")
    public ResponseEntity<Cliente> register(@RequestBody @Valid Cliente cliente){
        return clienteService.registrar(cliente);
    }

    @PutMapping
    @ApiOperation(value = "Atualiza o registro do cliente")
    public ResponseEntity<?> atualizar(@RequestBody @Valid Cliente cliente){
        return clienteService.atualizar(cliente);
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Remove um clientes através do Id")
    public ResponseEntity deletarPorId(@PathVariable Long id){
        return clienteService.deletarPorId(id);
    }

    @GetMapping(path = "/cpf/{cpf}")
    @ApiOperation(value = "Verificar se o CPF se encontra cadastrado na base de dados")
    public ResponseEntity<ClienteExistDTO> verificarSeClienteJaEstaCadastrado(@PathVariable String cpf){
        return clienteService.existsByCpf(cpf);
    }


}
