package com.br.jpw.JpwCreditClient.service;

import com.br.jpw.JpwCreditClient.domain.Cliente;
import com.br.jpw.JpwCreditClient.domain.ClienteExistDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface ClienteService {

    ResponseEntity<Page<Cliente>> listarTodos(Pageable pageable);

    ResponseEntity<Cliente> listarPorId(Long id);

    ResponseEntity<Cliente> registrar(Cliente cliente);

    ResponseEntity<?> atualizar(Cliente cliente);

    ResponseEntity deletarPorId(Long id);

    ResponseEntity<ClienteExistDTO> existsByCpf(String cpf);

}
