package com.br.jpw.JpwCreditClient.service;

import com.br.jpw.JpwCreditClient.domain.Cliente;
import com.br.jpw.JpwCreditClient.domain.ClienteExistDTO;
import com.br.jpw.JpwCreditClient.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImplementation implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public ResponseEntity<Page<Cliente>> listarTodos(Pageable pageable) {
        return ResponseEntity.status(HttpStatus.OK).body(clienteRepository.findAll(pageable));
    }

    @Override
    public ResponseEntity<Cliente> listarPorId(Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(clienteRepository.findById(id).get());
    }

    @Override
    public ResponseEntity<Cliente> registrar(Cliente cliente) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clienteRepository.save(cliente));
    }

    @Override
    public ResponseEntity<?> atualizar(Cliente cliente) {
        if(clienteRepository.findById(cliente.getId()).isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(clienteRepository.save(cliente));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                String.format("Resource not found for id %s", cliente.getId())
        );
    }

    @Override
    public ResponseEntity deletarPorId(Long id) {
        clienteRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Override
    public ResponseEntity<ClienteExistDTO> existsByCpf(String cpf) {
        if(clienteRepository.existsClienteByCpf(cpf)){
            return ResponseEntity.status(HttpStatus.OK).body(new ClienteExistDTO(cpf, Boolean.TRUE));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ClienteExistDTO(cpf, Boolean.FALSE));
    }

}
