package com.br.jpw.JpwCreditClient.repository;

import com.br.jpw.JpwCreditClient.domain.Cliente;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long> {

    Boolean existsClienteByCpf(String cpf);

}
