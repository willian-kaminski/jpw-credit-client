package com.br.jpw.JpwCreditClient.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Cliente {

    private static final long serialVersionUID = -1905907502453138175L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "Código do cliente")
    private Long id;

    @NotNull
    @Size(min = 10)
    @ApiModelProperty(value = "Nome do cliente")
    private String nome;

    @NotNull
    @ApiModelProperty(value = "Sexo do cliente")
    private Sexo sexo;

    @NotNull
    @ApiModelProperty(value = "RG do cliente")
    private String rg;

    @NotNull
    @Size(min = 11, max = 11)
    @ApiModelProperty(value = "CPF do cliente")
    private String cpf;

    @NotNull
    @ApiModelProperty(value = "Renda do cliente")
    private Double renda;

    @NotNull
    @Email
    @ApiModelProperty(value = "Email do cliente para contato")
    private String email;

    @NotNull
    @JsonFormat(pattern="YYYY-MM-dd")
    @ApiModelProperty(value = "Data de nascimento do cliente")
    private Date dataNascimento;

    @NotNull
    @ApiModelProperty(value = "Telefone do cliente para contato")
    private String telefone;

    @ApiModelProperty(value = "Dados de endereço do cliente")
    private Endereco endereco;

}
