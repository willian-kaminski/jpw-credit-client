package com.br.jpw.JpwCreditClient.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClienteExistDTO {

    private String cpf;
    private Boolean existe;

}
