package com.br.jpw.JpwCreditClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpwCreditClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpwCreditClientApplication.class, args);
	}

}
